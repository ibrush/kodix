<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
?><br><br><br><br><br><br><br><br><?
if(CModule::IncludeModule("iblock"))
{
    define('IBLOCK_TYPE', 'ibt_forms');
    define('IBLOCK_CODE', 'ib_form_1');

    $dbResult = CIBlockType::GetByID(IBLOCK_TYPE);
    $arResult = $dbResult->Fetch();
    // если нет типа инфоблоков, то созадем
    if (!$arResult)
    {
        $arFields = Array(
            'ID' => IBLOCK_TYPE,
            'SECTIONS'=>'N',
            'IN_RSS'=>'N',
            'LANG'=>Array(
                'ru'=>Array(
                    'NAME' => 'Формы'
                ),
                'en'=>Array(
                    'NAME' => 'Forms',
                )
            )
        );

        $obBlocktype = new CIBlockType;
        $res = $obBlocktype->Add($arFields);
        if(!$res)
        {
            die;
        }
    }

    // Проверяем создан ли инфоблок и создаем, с привязкой к текущщему сайту
    $dbResult = CIBlock::GetList(
        array(),
        array(
            'TYPE' => IBLOCK_TYPE,
            'CODE' => IBLOCK_CODE,
        ),
        false
    );
    if ($arIblock = $dbResult->Fetch())
    {
        echo "<pre>"; print_r($arIblock); echo "</pre>";
        $ID = $arIblock['ID'];
    }
    else
    {
        $ib = new CIBlock;
        $ibp = new CIBlockProperty;
        $arFields = Array(
            "ACTIVE" => "Y",
            "NAME" => "Тест",
            "CODE" => IBLOCK_CODE,
            "IBLOCK_TYPE_ID" => IBLOCK_TYPE,
            "SITE_ID" => Array(SITE_ID),
        );
        $ID = $ib->Add($arFields);

        if(!$ID)
        {
            die;
        }

        $arFields = Array(
            "NAME" => "опубликовано",
            "IS_REQUIRED" => "N",
            "ACTIVE" => "Y",
            "SORT" => "10",
            "CODE" => "PUBLIC",
            "PROPERTY_TYPE" => "S",
            "IBLOCK_ID" => $ID,
        );
        $PropID = $ibp->Add($arFields);
        $arFields = Array(
            "NAME" => "Имя",
            "IS_REQUIRED" => "Y",
            "ACTIVE" => "Y",
            "SORT" => "100",
            "CODE" => "NAME",
            "PROPERTY_TYPE" => "S",
            "IBLOCK_ID" => $ID,
        );
        $PropID = $ibp->Add($arFields);
        $arFields = Array(
            "NAME" => "Телефон",
            "IS_REQUIRED" => "Y",
            "ACTIVE" => "Y",
            "SORT" => "200",
            "CODE" => "PHONE",
            "PROPERTY_TYPE" => "S",
            "IBLOCK_ID" => $ID,
        );
        $PropID = $ibp->Add($arFields);
        $arFields = Array(
            "NAME" => "Email",
            "IS_REQUIRED" => "Y",
            "ACTIVE" => "Y",
            "SORT" => "300",
            "CODE" => "EMAIL",
            "PROPERTY_TYPE" => "S",
            "IBLOCK_ID" => $ID,
        );
        $PropID = $ibp->Add($arFields);
        $arFields = Array(
            "NAME" => "Индекс",
            "IS_REQUIRED" => "N",
            "ACTIVE" => "Y",
            "SORT" => "400",
            "CODE" => "POST",
            "PROPERTY_TYPE" => "N",
            "IBLOCK_ID" => $ID,
        );
        $PropID = $ibp->Add($arFields);
        $arFields = Array(
            "NAME" => "Город",
            "IS_REQUIRED" => "N",
            "ACTIVE" => "Y",
            "SORT" => "500",
            "CODE" => "CITY",
            "PROPERTY_TYPE" => "L",
            "IBLOCK_ID" => $ID,
            "VALUES" => array(
                array(
                    "VALUE" => "Москва",
                    "DEF" => "N",
                    "SORT" => "100"
                ),
                array(
                    "VALUE" => "Владимир",
                    "DEF" => "N",
                    "SORT" => "200"
                ),
                array(
                    "VALUE" => "Тула",
                    "DEF" => "N",
                    "SORT" => "300"
                ),
                array(
                    "VALUE" => "Тверь",
                    "DEF" => "N",
                    "SORT" => "400"
                ),
            )
        );
        $PropID = $ibp->Add($arFields);

        $arFields = Array(
            "NAME" => "Адрес",
            "IS_REQUIRED" => "Y",
            "ACTIVE" => "Y",
            "SORT" => "600",
            "CODE" => "ADDRESS",
            "PROPERTY_TYPE" => "S",
            "IBLOCK_ID" => $ID,
            "ROW_COUNT" => 5,
            "USER_TYPE" => "",
        );
        $PropID = $ibp->Add($arFields);

        $arFields = Array(
            "NAME" => "Тема обращения",
            "IS_REQUIRED" => "N",
            "ACTIVE" => "Y",
            "SORT" => "700",
            "CODE" => "TYPE",
            "PROPERTY_TYPE" => "L",
            "IBLOCK_ID" => $ID,
            "VALUES" => array(
                array(
                    "VALUE" => "вопрос",
                    "DEF" => "N",
                    "SORT" => "100"
                ),
                array(
                    "VALUE" => "жалоба",
                    "DEF" => "N",
                    "SORT" => "200"
                ),
                array(
                    "VALUE" => "предложение",
                    "DEF" => "N",
                    "SORT" => "300"
                ),
                array(
                    "VALUE" => "другая",
                    "DEF" => "Y",
                    "SORT" => "400"
                ),
            )
        );
        $PropID = $ibp->Add($arFields);
        $arFields = Array(
            "NAME" => "Сообщение",
            "IS_REQUIRED" => "Y",
            "ACTIVE" => "Y",
            "SORT" => "800",
            "CODE" => "MESSAGE",
            "PROPERTY_TYPE" => "S",
            "IBLOCK_ID" => $ID,
            "ROW_COUNT" => 5,
            "USER_TYPE" => "",
        );
        $PropID = $ibp->Add($arFields);

        $arFields = Array(
            "NAME" => "Согласны ли вы на обработку ваших персональных данных",
            "IS_REQUIRED" => "Y",
            "ACTIVE" => "Y",
            "SORT" => "900",
            "CODE" => "AGREE",
            "PROPERTY_TYPE" => "L",
            "LIST_TYPE" => "C",
            "MULTIPLE" => "Y",
            "IBLOCK_ID" => $ID,
            "VALUES" => array(
                array(
                    "VALUE" => "Y",
                    "DEF" => "N",
                    "SORT" => "100"
                ),
            )
        );
        $PropID = $ibp->Add($arFields);
    }
}


?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>